package view;

import game.Background;
import game.Keyboard;
import controller.LevelController;
import gameElements.activeElements.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import gameElements.activeElements.Character;
import gameElements.staticElements.*;
import utils.Res;

public class Platform extends JPanel {

    private static final int MIN_LEVEL_X = 0;
    private static final int MAX_LEVEL_X = 4600;
    private static final int MIN_BACKGROUND_X = -800;
    private static final int MAX_BACKGROUND_X = 800;
    private static final int START_FIRST_BACKGROUND = -50;
    private static final int START_SECOND_BACKGROUND = 750;

    private int movement;
    private int xPosition;
    private int floorOffsetY;
    private int heightLimit;
    private Mario mario;
    private Background firstBackground;
    private Background secondBackground;
    private List<Obstacle> objects = new ArrayList<>();
    private List<Obstacle> coins = new ArrayList<>();
    private List<Character> enemies = new ArrayList<>();
    private LevelController levelController;

    public Platform() {
        super();
        this.movement = 0;
        this.xPosition = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.firstBackground = new Background(START_FIRST_BACKGROUND);
        this.secondBackground = new Background(START_SECOND_BACKGROUND);
        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard(this));
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMovement() {
        return movement;
    }

    public int getXPosition() {
        return xPosition;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setXPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public Mario getMario(){
        return this.mario;
    }

    private void movingScreen(){
        if (this.xPosition >= MIN_LEVEL_X && this.xPosition <= MAX_LEVEL_X) {
            this.xPosition = this.xPosition + this.movement;
            this.firstBackground.setBackground(this.firstBackground.getBackground()-this.movement);
            this.secondBackground.setBackground(this.secondBackground.getBackground()-this.movement);
        }
    }

    public void setObjects(LevelController itemsInTheLevel){
        this.levelController = itemsInTheLevel;
        this.mario = (Mario) levelController.getMario();
        this.coins = levelController.getCoinsElements();
        this.objects = levelController.getStaticElements();
        this.enemies = levelController.getActiveElements();
    }

    private void changingBackground(){
        if (this.firstBackground.getBackground() == MIN_BACKGROUND_X) {
            this.firstBackground.setBackground(MAX_BACKGROUND_X);
        }else if (this.secondBackground.getBackground() ==MIN_BACKGROUND_X) {
            this.secondBackground.setBackground(MAX_BACKGROUND_X);
        }else if (this.firstBackground.getBackground() == MAX_BACKGROUND_X) {
            this.firstBackground.setBackground(MIN_BACKGROUND_X);
        }else if (this.secondBackground.getBackground() == MAX_BACKGROUND_X) {
            this.secondBackground.setBackground(MIN_BACKGROUND_X);
        }
    }

    private void updateBackgroundOnMovement() {
        movingScreen();
        changingBackground();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.levelController.doAllTheChecks();
        this.updateBackgroundOnMovement();
        this.drawAllCharactersAndObjects(g);
        this.drawMarioIfJumping(g);
        this.drawEnemiesIfAliveOrNot(g);
    }

    private void drawAllCharactersAndObjects(Graphics g){
        g.drawImage(firstBackground.getImageBackground(), firstBackground.getBackground(), MIN_LEVEL_X, null);
        g.drawImage(secondBackground.getImageBackground(), secondBackground.getBackground(), MIN_LEVEL_X, null);

        for (Obstacle object : objects) {
            g.drawImage(object.getImgObj(), object.getPosition().getX(),
                    object.getPosition().getY(), null);
        }

        for (Obstacle coin : coins) {
            g.drawImage(coin.getImgObj(), coin.getPosition().getX(),
                    coin.getPosition().getY(), null);
        }
    }

    private void drawMarioIfJumping(Graphics g){
        if (this.mario.isJumping())
            g.drawImage(this.mario.doJump(), this.mario.getPosition().getX(), this.mario.getPosition().getY(), null);
        else
            g.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO, this.mario.getCharacterFrequency()), this.mario.getPosition().getX(), this.mario.getPosition().getY(), null);
    }

    private void drawEnemiesIfAliveOrNot(Graphics g){
        for(Character enemy: enemies) {
            if (enemy.isAlive())
                g.drawImage(enemy.walk(enemy.resImageAlive(), enemy.getCharacterFrequency()), enemy.getPosition().getX(),enemy.getPosition().getY(), null);
            else
                g.drawImage(enemy.deadImage(), enemy.getPosition().getX(), enemy.getPosition().getY() + enemy.getCharacterOffset(), null);
        }
    }
}
