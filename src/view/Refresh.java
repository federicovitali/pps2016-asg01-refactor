package view;

import view.Platform;

public class Refresh implements Runnable {

    private static final int PAUSE = 4;
    private Platform level;

    public Refresh(Platform level){
        this.level = level;
    }

    public void run() {
        while (true) {
            this.level.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

} 
