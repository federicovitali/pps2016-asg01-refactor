package gameElements.staticElements;

import java.awt.Image;
import view.Platform;
import utils.Position;

public abstract class ObstacleImpl implements Obstacle{

    private int width, height;
    private Platform level;
    Image imgObj;
    private Position gamePosition;


    public ObstacleImpl(Position gamePosition, int width, int height, Platform level) {
        this.gamePosition = gamePosition;
        this.width = width;
        this.height = height;
        this.level = level;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Position getPosition() {
        return this.gamePosition;
    }

    public Image getImgObj() {
        return imgObj;
    }

    public void move() {
        if (this.level.getXPosition() >= 0) {
            this.gamePosition.setX(this.gamePosition.getX()-this.level.getMovement());
        }
    }

}
