package gameElements.staticElements;

import view.Platform;
import utils.Position;
import utils.Res;
import utils.Utils;

public class Block extends ObstacleImpl {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;

    public Block(Position gamePosition, Platform level) {
        super(gamePosition, WIDTH, HEIGHT,level);
        super.imgObj = Utils.getImage(Res.IMG_BLOCK);
    }

}
