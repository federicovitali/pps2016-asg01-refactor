package gameElements.staticElements;

import gameElements.GameElement;

import java.awt.*;

/**
 * Created by Federico on 12/03/2017.
 */
public interface Obstacle extends GameElement {

    Image getImgObj();
}