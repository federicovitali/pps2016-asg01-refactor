package gameElements.staticElements;

import view.Platform;
import utils.Position;
import utils.Res;
import utils.Utils;

/**
 * Created by Federico on 12/03/2017.
 */
public class StartPlaque extends ObstacleImpl {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;

    public StartPlaque(Position gamePosition, Platform level) {
        super(gamePosition, WIDTH, HEIGHT,level);
        super.imgObj = Utils.getImage(Res.START_ICON);

    }
}
