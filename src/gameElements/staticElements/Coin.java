package gameElements.staticElements;

import view.Platform;
import utils.Position;
import utils.Res;
import utils.Utils;

import java.awt.Image;

public class Coin extends ObstacleImpl implements Runnable {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 150;
    private int counter;

    public Coin(Position gamePosition, Platform level) {
        super(gamePosition, WIDTH, HEIGHT,level);
        super.imgObj = Utils.getImage(Res.IMG_PIECE1);
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
