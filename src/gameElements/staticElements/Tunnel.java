package gameElements.staticElements;

import view.Platform;
import utils.Position;
import utils.Res;
import utils.Utils;

public class Tunnel extends ObstacleImpl {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;

    public Tunnel(Position gamePosition, Platform level) {
        super(gamePosition, WIDTH, HEIGHT,level);
        super.imgObj = Utils.getImage(Res.IMG_TUNNEL);
    }

}
