package gameElements.activeElements;

import java.awt.Image;

import view.Platform;
import gameElements.GameElement;
import utils.Position;
import utils.Res;
import utils.Utils;

public class Turtle extends BasicCharacter implements Runnable {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;
    private static final int PAUSE = 15;
    private static final int TURTLE_FREQUENCY = 10;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private int dxTurtle;
    private String resImageAlive;
    private Position gamePosition;


    public Turtle(Position gamePosition, Platform level) {
        super(gamePosition, WIDTH, HEIGHT, level);
        super.setToRight(true);
        super.setMoving(true);
        this.dxTurtle = 1;
        resImageAlive = Res.IMGP_CHARACTER_TURTLE;
        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
        this.gamePosition = gamePosition;
    }

    public void move() {
        this.dxTurtle = isToRight() ? 1 : -1;
        gamePosition.setX(gamePosition.getX() + this.dxTurtle);
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void contact(GameElement object) {
        if (this.hitAhead(object) && this.isToRight()) {
            this.setToRight(false);
            this.dxTurtle = -1;
        } else if (this.hitBack(object) && !this.isToRight()) {
            this.setToRight(true);
            this.dxTurtle = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }

    public String resImageAlive() {
        return this.resImageAlive;
    }

    @Override
    public int getCharacterFrequency() {
        return TURTLE_FREQUENCY;
    }

    @Override
    public int getCharacterOffset() {
        return TURTLE_DEAD_OFFSET_Y;
    }
}