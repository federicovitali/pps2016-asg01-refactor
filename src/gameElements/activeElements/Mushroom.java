package gameElements.activeElements;

import java.awt.Image;

import view.Platform;
import gameElements.GameElement;
import utils.Position;
import utils.Res;
import utils.Utils;

public class Mushroom extends BasicCharacter implements Runnable {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 15;
    private static final int MUSHROOM_FREQUENCY = 10;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private int offsetX;
    private String resImageAlive;
    private Position gamePosition;


    public Mushroom(Position gamePosition, Platform level) {
        super(gamePosition, WIDTH, HEIGHT, level);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;
        this.resImageAlive = Res.IMGP_CHARACTER_MUSHROOM;
        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
        this.gamePosition = gamePosition;
    }

    public void move() {
        this.offsetX = isToRight() ? 1 : -1;
        gamePosition.setX(gamePosition.getX() + this.offsetX);
    }

    @Override
    public void run() {
        while (true) {
            if (this.alive) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void contact(GameElement obj) {
        if (this.hitAhead(obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage() {
        return Utils.getImage(this.isToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }

    public String resImageAlive() {
        return this.resImageAlive;
    }


    @Override
    public int getCharacterFrequency() {
        return MUSHROOM_FREQUENCY;
    }

    @Override
    public int getCharacterOffset() {
        return MUSHROOM_DEAD_OFFSET_Y;
    }
}
