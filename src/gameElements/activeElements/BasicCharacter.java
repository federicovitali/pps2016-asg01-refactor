package gameElements.activeElements;

import java.awt.Image;
import view.Platform;
import gameElements.GameElement;
import utils.Position;
import utils.Res;
import utils.Utils;

public abstract class BasicCharacter implements Character {

    private static final int PROXIMITY_MARGIN = 10;
    private int width, height;
    private boolean moving;
    private boolean toRight;
    private int counter;
    private Platform level;
    private Position gamePosition;
    boolean alive;

    public BasicCharacter(Position gamePosition, int width, int height, Platform level) {
        this.gamePosition = gamePosition;
        this.height = height;
        this.width = width;
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
        this.level = level;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Position getPosition() {
        return this.gamePosition;
    }

    public boolean isAlive() {
        return alive;
    }

    @Override
    public abstract Image deadImage();

    @Override
    public abstract String resImageAlive();

    @Override
    public abstract int getCharacterFrequency();

    @Override
    public abstract int getCharacterOffset();

    boolean isToRight() {
        return toRight;
    }

    void setAlive() {
        this.alive = false;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public void move() {
        if (level.getXPosition() >= 0) {
            gamePosition.setX(gamePosition.getX() - level.getMovement());
        }
    }

    public boolean hitAbove(GameElement object) {
        return !(gamePosition.getX() + this.width < object.getPosition().getX() + 5 || gamePosition.getX() > object.getPosition().getX() + object.getWidth() - 5 ||
                gamePosition.getY() < object.getPosition().getY() + object.getHeight() || gamePosition.getY() > object.getPosition().getY() + object.getHeight() + 5);
    }

    public boolean hitAhead(GameElement character) {
        return this.isToRight() && !(gamePosition.getX() + this.width < character.getPosition().getX() || gamePosition.getX() + this.width > character.getPosition().getX() + 5 ||
                gamePosition.getY() + this.height <= character.getPosition().getY() ||gamePosition.getY() >= character.getPosition().getY() + character.getHeight());
    }

    public boolean hitBack(GameElement character) {
        return !(gamePosition.getX() > character.getPosition().getX() + character.getWidth() || gamePosition.getX() + this.width < character.getPosition().getX() + character.getWidth() - 5 ||
                gamePosition.getY() + this.height <= character.getPosition().getY() || gamePosition.getY() >= character.getPosition().getY() + character.getHeight());
    }

    @Override
    public abstract void contact(GameElement object);

    public boolean hitBelow(GameElement character) {
        return !(gamePosition.getX() + this.width < character.getPosition().getX() || gamePosition.getX() > character.getPosition().getX() + character.getWidth() ||
                gamePosition.getY() + this.height < character.getPosition().getY() || gamePosition.getY() + this.height > character.getPosition().getY());
    }

    public boolean isNearby(GameElement object) {
        return (gamePosition.getX() > object.getPosition().getX() - PROXIMITY_MARGIN && gamePosition.getX()< object.getPosition().getX() + object.getWidth() + PROXIMITY_MARGIN) ||
                (gamePosition.getX() + this.width > object.getPosition().getX() - PROXIMITY_MARGIN && gamePosition.getX() + this.width < object.getPosition().getX() + object.getWidth() + PROXIMITY_MARGIN);
    }
}
