package gameElements.activeElements;

import gameElements.GameElement;

import java.awt.Image;

public interface Character extends GameElement{
	Image walk(String name, int frequency);

	boolean isNearby(GameElement object);

	boolean hitBelow(GameElement character);

	void move();

	boolean hitAbove(GameElement object);

	boolean hitAhead(GameElement character);

	boolean hitBack(GameElement character);

	void contact(GameElement object);

	boolean isAlive();

	Image deadImage();

	String resImageAlive();

	int getCharacterFrequency();

	int getCharacterOffset();
}
