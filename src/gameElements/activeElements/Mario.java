package gameElements.activeElements;

import java.awt.Image;

import view.Platform;
import gameElements.GameElement;
import gameElements.staticElements.ObstacleImpl;
import gameElements.staticElements.Coin;
import utils.Position;
import utils.Res;
import utils.Utils;

public class Mario extends BasicCharacter {

    private static final int MARIO_FREQUENCY = 100;
    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;
    private static final int DOWN_OFFSET_Y = 4;
    private Platform level;
    private boolean jumping;
    private int jumpingExtent;
    private Position gamePosition;

    public Mario(Position gamePosition, Platform level) {
        super(gamePosition, WIDTH, HEIGHT, level);
        Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
        this.level = level;
        this.gamePosition = gamePosition;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping() {
        this.jumping = true;
    }

    public Image doJump() {
        String resImage;
        this.jumpingExtent++;
        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (gamePosition.getY() > level.getHeightLimit()) {
                gamePosition.setY(gamePosition.getY() - DOWN_OFFSET_Y);
            }else{
                this.jumpingExtent = JUMPING_LIMIT;
            }
            resImage = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        }else if (gamePosition.getY() + this.getHeight() < level.getFloorOffsetY()) {
            gamePosition.setY(gamePosition.getY() + 1);
            resImage = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            resImage = this.isToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return Utils.getImage(resImage);
    }

    private void contactObstacle(ObstacleImpl object) {
        if (this.hitAhead(object) && this.isToRight() || this.hitBack(object) && !this.isToRight()) {
            level.setMovement(0);
            this.setMoving(false);
        }
        if (this.hitBelow(object) && this.jumping) {
            level.setFloorOffsetY(object.getPosition().getY());
        } else if (!this.hitBelow(object)) {
            level.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            if (!this.jumping) {
                gamePosition.setY(MARIO_OFFSET_Y_INITIAL);
            }
            if (hitAbove(object)) {
                level.setHeightLimit(object.getPosition().getY() + object.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(object) && !this.jumping) {
                level.setHeightLimit(0); // initial sky
            }
        }
    }

    public boolean contactPiece(Coin coin) {
        return this.hitBack(coin) || this.hitAbove(coin) || this.hitAhead(coin)
                || this.hitBelow(coin);

    }

    private void contactCharacter(BasicCharacter character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.alive) {
            this.setMoving(false);
            this.setAlive();
            }
        } else if (this.hitBelow(character)) {
            character.setMoving(false);
            character.setAlive();
        }
    }

    @Override
    public Image deadImage() {
        return null;
    }

    @Override
    public String resImageAlive() {
        return Res.IMG_MARIO_DEFAULT;
    }

    @Override
    public int getCharacterFrequency() {
        return MARIO_FREQUENCY;
    }

    @Override
    public int getCharacterOffset() {
        return MARIO_OFFSET_Y_INITIAL;
    }

    @Override
    public void contact(GameElement object) {
        if (object instanceof  BasicCharacter){
            contactCharacter((BasicCharacter) object);
        }else if (object instanceof  ObstacleImpl){
            contactObstacle((ObstacleImpl) object);
        }
    }
}
