package gameElements;

import view.Platform;
import gameElements.activeElements.Character;
import gameElements.activeElements.Mario;
import gameElements.activeElements.Mushroom;
import gameElements.activeElements.Turtle;
import gameElements.staticElements.*;
import utils.Position;

/**
 * Created by Federico on 16/03/2017.
 */
public class FirstWorldFactory implements AbstractGameElementsFactory {
    @Override
    public Character createMainCharacter(Position position, Platform level) {
        return new Mario(position,level);
    }

    @Override
    public Character createMushroom(Position position, Platform level) {
        return new Mushroom(position,level);
    }

    @Override
    public Character createTurtle(Position position, Platform level) {
        return new Turtle(position,level);
    }

    @Override
    public Obstacle createTunnel(Position position, Platform level) {
        return new Tunnel(position,level);
    }

    @Override
    public Obstacle createCoin(Position position, Platform level) {
        return new Coin(position,level);
    }

    @Override
    public Obstacle createBlock(Position position, Platform level) {
        return new Block(position,level);
    }

    @Override
    public Obstacle createFlag(Position position, Platform level) {
        return new Flag(position,level);
    }

    @Override
    public Obstacle createCastle(Position position, Platform level) {
        return new Castle(position,level);
    }

    @Override
    public Obstacle createPlaque(Position position, Platform level) {
        return new StartPlaque(position,level);
    }
}
