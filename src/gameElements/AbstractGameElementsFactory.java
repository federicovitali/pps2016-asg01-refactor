package gameElements;

import view.Platform;
import gameElements.activeElements.Character;
import gameElements.staticElements.*;
import utils.Position;

/**
 * Created by Federico on 16/03/2017.
 */
public interface AbstractGameElementsFactory {

    Character createMainCharacter(Position position, Platform level);
    Character createMushroom(Position position, Platform level);
    Character createTurtle(Position position, Platform level);
    Obstacle createTunnel(Position position, Platform level);
    Obstacle createCoin(Position position, Platform level);
    Obstacle createBlock(Position position, Platform level);
    Obstacle createFlag(Position position, Platform level);
    Obstacle createCastle(Position position, Platform level);
    Obstacle createPlaque(Position position, Platform level);



}
