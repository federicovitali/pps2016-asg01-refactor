package gameElements;

import utils.Position;

/**
 * Created by Federico on 12/03/2017.
 */
public interface GameElement {
    int getWidth();

    int getHeight();

    Position getPosition();

    void move();
}
