package utils;

/**
 * Created by Federico on 14/03/2017.
 */
public interface Point {

    int getX();

    int getY();

    void setX(int X);

    void setY(int Y);

}
