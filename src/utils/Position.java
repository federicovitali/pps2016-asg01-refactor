package utils;

/**
 * Created by Federico on 14/03/2017.
 */
public class Position implements Point{

    private int X;
    private int Y;

    public Position(int X, int Y){
        this.X = X;
        this.Y = Y;
    }

    @Override
    public int getX() {
        return X;
    }

    @Override
    public int getY() {
        return Y;
    }

    @Override
    public void setX(int X) {
        this.X = X;
    }

    @Override
    public void setY(int Y) {
        this.Y = Y;

    }
}
