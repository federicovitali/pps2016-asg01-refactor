package game;

import gameElements.AbstractGameElementsFactory;
import gameElements.FirstWorldFactory;
import controller.LevelController;
import view.Platform;
import view.Refresh;

import javax.swing.*;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final int NUMBERS_OF_TUNNEL = 10;
    private static final int NUMBERS_OF_BLOCK = 15;
    private static final int NUMBERS_OF_PIECE = 7;
    private static final int NUMBERS_OF_ENEMIES = 3;
    private static final String WINDOW_TITLE = "Super Mario";
    private static AbstractGameElementsFactory gameFactory = new FirstWorldFactory();
    private static Platform scene;

    public static void main(String[] args) {
        JFrame panel = new JFrame(WINDOW_TITLE);
        panel.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        panel.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        panel.setLocationRelativeTo(null);
        panel.setResizable(true);
        panel.setAlwaysOnTop(true);
        scene = new Platform();
        LevelController objectsInTheLevel = new LevelController(gameFactory,NUMBERS_OF_TUNNEL,NUMBERS_OF_BLOCK,NUMBERS_OF_PIECE,NUMBERS_OF_ENEMIES,scene);
        scene.setObjects(objectsInTheLevel);
        panel.setContentPane(scene);
        panel.setVisible(true);

        Thread timer = new Thread(new Refresh(scene));
        timer.start();
    }
}
