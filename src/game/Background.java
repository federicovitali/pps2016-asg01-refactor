package game;

import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by Federico on 12/03/2017.
 */
public class Background {
    private int positionX;
    private Image imageBackground;

    public Background(int positionX){
        this.positionX = positionX;
        this.imageBackground = Utils.getImage(Res.IMG_BACKGROUND);
    }

    public void setBackground(int x) {
        this.positionX = x;
    }

    public int getBackground(){
        return this.positionX;
    }

    public Image getImageBackground() {
        return this.imageBackground;
    }

}
