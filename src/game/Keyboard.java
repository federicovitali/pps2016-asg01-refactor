package game;

import view.Platform;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    private Platform platform;

    public Keyboard(Platform platform){
        this.platform = platform;
    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (platform.getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                if (platform.getXPosition() == -1) {
                    platform.setXPosition(0);
                }
                platform.getMario().setMoving(true);
                platform.getMario().setToRight(true);
                platform.setMovement(1);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                if (platform.getXPosition() == 4601) {
                    platform.setXPosition(4600);
                }
                platform.getMario().setMoving(true);
                platform.getMario().setToRight(false);
                platform.setMovement(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                platform.getMario().setJumping();
                Audio.playSound("/resources/audio/jump.wav");
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        platform.getMario().setMoving(false);
        platform.setMovement(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
