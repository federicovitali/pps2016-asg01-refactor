package controller;

import game.Audio;
import gameElements.AbstractGameElementsFactory;
import view.Platform;
import gameElements.activeElements.Character;
import gameElements.activeElements.Mario;
import gameElements.staticElements.*;
import utils.Position;
import utils.Res;
import utils.Utils;
import java.util.*;

/**
 * Created by Federico on 16/03/2017.
 */
public class LevelController {

    private int numbersOfTunnel;
    private int numbersOfBlock;
    private int numbersOfPiece;
    private int numbersOfEnemies;
    private static final int TYPE_WITH_SPACING = 1;
    private static final int TYPE_WITH_NO_SPACING = 0;
    private static final int MIN_LEVEL_X = 0;
    private static final int MAX_LEVEL_X = 4600;
    private Platform level;
    private AbstractGameElementsFactory gameFactory;
    private List<Obstacle> passiveObjects = new ArrayList<>();
    private List<Obstacle> coninsInTheLevel = new ArrayList<>();
    private List<Character> enemies = new ArrayList<>();
    private Character mainCharacter;

    public LevelController(AbstractGameElementsFactory gameFactory, int numbersOfTunnel, int numbersOfBlock, int numbersOfPiece, int numbersOfEnemies, Platform level){
        this.gameFactory = gameFactory;
        this.numbersOfBlock = numbersOfBlock;
        this.numbersOfEnemies = numbersOfEnemies;
        this.numbersOfPiece = numbersOfPiece;
        this.numbersOfTunnel = numbersOfTunnel;
        this.level = level;
        createElements();
    }

    private void createElements(){
        for (int i = 0; i < numbersOfBlock; i++) {
            this.passiveObjects.add(gameFactory.createBlock(new Position(Utils.getRandomCoordinate(4600,400, TYPE_WITH_SPACING),Utils.getRandomCoordinate(210,160, TYPE_WITH_NO_SPACING)),level));
        }
        for (int i = 0; i < numbersOfPiece; i++) {
            this.coninsInTheLevel.add(gameFactory.createCoin(new Position(Utils.getRandomCoordinate(4600,400, TYPE_WITH_SPACING),Utils.getRandomCoordinate(145,45, TYPE_WITH_NO_SPACING)),level));
        }
        for (int i = 0; i < numbersOfTunnel; i++) {
            this.passiveObjects.add(gameFactory.createTunnel(new Position(Utils.getRandomCoordinate(4600,400, TYPE_WITH_SPACING),230),level));
        }
        for (int i = 0; i < numbersOfEnemies; i++) {
            this.enemies.add(gameFactory.createMushroom(new Position(Utils.getRandomCoordinate(4600,400, TYPE_WITH_SPACING),263),level));
            this.enemies.add(gameFactory.createTurtle(new Position(Utils.getRandomCoordinate(4600,400, TYPE_WITH_SPACING),243),level));
        }
        this.passiveObjects.add(gameFactory.createCastle(new Position(10,95),level));
        this.passiveObjects.add(gameFactory.createCastle(new Position(4850,95),level));
        this.passiveObjects.add(gameFactory.createPlaque(new Position(220,234),level));
        this.passiveObjects.add(gameFactory.createFlag(new Position(4650,115),level));
        this.mainCharacter=gameFactory.createMainCharacter(new Position(300, 245),level);
    }

    public List<Character> getActiveElements(){
        return Collections.unmodifiableList(this.enemies);
    }

    public Character getMario(){
        return this.mainCharacter;
    }

    public List<Obstacle> getCoinsElements(){
        return this.coninsInTheLevel;
    }

    public List<Obstacle> getStaticElements(){
        return Collections.unmodifiableList(this.passiveObjects);
    }

    public void doAllTheChecks(){
        this.checkContactWithObjects();
        this.checkMarioPickMoney();
        this.checkContactBetweenCharacters();
        this.moveObjectsAndCharacters();
    }

    private void checkContactWithObjects(){
        for (Obstacle object : passiveObjects) {
            if (this.mainCharacter.isNearby(object))
                this.mainCharacter.contact(object);
            for(Character enemy : enemies)
                if (enemy.isNearby(object))
                    enemy.contact(object);
        }
    }

    private void checkContactBetweenCharacters(){
        for (int i = 0; i < enemies.size(); i++) {
            for (int j = i+1; j < enemies.size(); j++) {
                if (enemies.get(i).isNearby(enemies.get(j))) {
                    enemies.get(i).contact(enemies.get(j));
                }
            }
            if (this.mainCharacter.isNearby(enemies.get(i))) {
                this.mainCharacter.contact(enemies.get(i));
            }
        }
    }

    private void checkMarioPickMoney(){
        for (int i = 0; i < coninsInTheLevel.size(); i++) {
            if (this.mainCharacter instanceof  Mario){
                Mario mario = (Mario) mainCharacter;
                if (mario.contactPiece((Coin) this.coninsInTheLevel.get(i))) {
                    Audio.playSound(Res.AUDIO_MONEY);
                    this.coninsInTheLevel.remove(i);
                }
            }
        }
    }

    private void moveObjectsAndCharacters(){
        if (this.level.getXPosition() >= MIN_LEVEL_X && this.level.getXPosition() <= MAX_LEVEL_X) {
            for (Obstacle object : passiveObjects) {
                object.move();
            }
            for (Obstacle coin : coninsInTheLevel) {
                coin.move();
            }
            for (Character character:enemies){
                character.move();
            }
        }
    }

}
